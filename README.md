Lights & sound installation [*The rocket that bumped off the ceiling*](https://hotel.schauspielhaus.at/events/max-windisch-spoerk-the-rocket-that-bumped-off-the-ceiling/), by Max Windisch-Spork and Adrian Artacho.

![Room107:yellow](https://docs.google.com/drawings/d/e/2PACX-1vQGTV5FAtWrOZTAjeEq3_uNZ83b3N09d0kMZELXbZj8c3xbDLIcxOJmnnNlYf06CZVN7zyC2cpodg7R/pub?w=479&h=655)


Dieses Werk wurde mit freundlicher Unterstützung des österreichischen Bundesministeriums für Kunst, Kultur, öffentlicher Dienst und Sport realisiert.

![BMKÖES](https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/BMKOES_AT_Logo.svg/640px-BMKOES_AT_Logo.svg.png)

[www.bmkoes.gv.at](https://www.bmkoes.gv.at/)

# Max For Live Devices

Here is the [online repository]([Bitbucket](https://bitbucket.org/AdrianArtacho/therocketthatbumpedofftheceiling/src/master/)) for all the M4L devices developed during the work on the installation.



# MidiLights

Midilights are controlled using the [DMXIS](https://www.youtube.com/watch?v=XAR8TBkQQZ8) interface. 

Lowest note (bank) is C-2 (Ch15?); Ch16 for Presets?


